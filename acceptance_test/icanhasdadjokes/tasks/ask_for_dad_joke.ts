import {Task} from '@serenity-js/core/lib/screenplay';
import {Get} from '../../support/interactions/get';

export const AsksForADadJoke = (id: string) => Task.where(`#actor wants to get a Dad joke`,
    Get.resource('/j/' + id),
);

export const AsksForARandomDadJoke = () => Task.where(`#actor wants to get a Dad joke`,
    Get.resource('/'),
);
